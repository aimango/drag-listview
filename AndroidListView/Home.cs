﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace TablesAndCellStyles {
    [Activity(Label = "TablesAndCellStyles", MainLauncher = true)]
    public class Home : Activity {
        
        protected override void OnCreate(Bundle bundle)
        {
	        base.OnCreate(bundle);
	        SetContentView(Resource.Layout.activity_list_view);
	
	        JavaList<string>mCheeseList = new JavaList<string>();
	        for (int i = 0; i < Cheeses.sCheeseStrings.Length; ++i) {
	            mCheeseList.Add(Cheeses.sCheeseStrings[i]);
	        }
	
	        StableArrayAdapter adapter = new StableArrayAdapter(this, Resource.Layout.text_view, mCheeseList);
			DynamicListView listView = FindViewById<DynamicListView>(Resource.Id.listview);
	        listView.setCheeseList(mCheeseList);
			listView.Adapter = adapter;
			listView.ChoiceMode = ChoiceMode.Single;
		}
	}
}